
package packegepricipal;

/**
 *
 * @author tavo
 */
import javax.persistence.*;
import entidad.*;
import java.util.Date;
public class ClasePrincipal 
{
    public static void main(String args[])
    {
        EntityManagerFactory Emf = Persistence.createEntityManagerFactory("TareaLineaOrdenPU");
        EntityManager em = Emf.createEntityManager();
        em.getTransaction().begin();
        Date fecha = new Date();
        
        Producto producto1 = new Producto(); //producto1
        producto1.setIdProducto(1);
        producto1.setDescripcion("Rfresco Coca Cola");
        producto1.setPrecio(8.50f);
        Producto producto2 = new Producto(); //producto2
        producto2.setIdProducto(2);
        producto2.setDescripcion("Rfresco Pepsi 2 Lts");
        producto2.setPrecio(20.53f);
        Producto producto3 = new Producto();//producto3
        producto3.setIdProducto(3);
        producto3.setDescripcion("Refreso Peñafiel 1 L ");
        producto3.setPrecio(13.50f);
       
        
        Orden orden1= new Orden();//orden1
        orden1.setIdOrden(1);
        orden1.setFechaOrden(fecha);
        Orden orden2= new Orden();//orden2
        orden2.setIdOrden(2);
        orden2.setFechaOrden(fecha);
        Orden orden3= new Orden();//orden3
        orden3.setIdOrden(3);
        orden3.setFechaOrden(fecha);
        
        Lineaorden lineorden1= new Lineaorden(1,1);//l1
        lineorden1.setCantidad(4);       
        Lineaorden lineorden2= new Lineaorden(1,2);//l2
        lineorden2.setCantidad(2);
        Lineaorden lineorden3= new Lineaorden(2,1);//l3
        lineorden3.setCantidad(1);
        Lineaorden lineord4= new Lineaorden(2,2);//l4
        lineord4.setCantidad(6);
        try {
            //insertando en procuto
            em.persist(producto1);
            em.persist(producto2);
            em.persist(producto3);
            //insertando en orden
            em.persist(orden1);
            em.persist(orden2);
            em.persist(orden3);
            //insertando en lineaorden
            em.persist(lineorden1);
            em.persist(lineorden2);
            em.persist(lineorden3);
            em.persist(lineord4);
        } catch (Exception e) {
            System.out.println("Eror al insertar, favor de checar" + e.getMessage()); 
           }
        
        System.out.println("Catalogo de Productos ");
        int producto1Id = producto1.getIdProducto();
        int producto2Id = producto2.getIdProducto();
        int producto3Id = producto3.getIdProducto();
        
        Producto dbProducto1 = em.find(Producto.class, producto1Id);
        System.out.println("Datos del Producto 1:");
        System.out.println("\t Id: " + dbProducto1.getIdProducto());
        System.out.println("\t Descripción: " + dbProducto1.getDescripcion());
        System.out.println("\t Precio: " + dbProducto1.getPrecio());
        
        Producto dbProducto2 = em.find(Producto.class, producto2Id);
        System.out.println("Datos del Producto 2:");
        System.out.println("\t Id: " + dbProducto2.getIdProducto());
        System.out.println("\t Descripción: " + dbProducto2.getDescripcion());
        System.out.println("\t Precio: " + dbProducto2.getPrecio());
        
        Producto dbProducto3 = em.find(Producto.class, producto3Id);
        System.out.println("Datos del PrOducto 3:");
        System.out.println("\t Id: " + dbProducto3.getIdProducto());
        System.out.println("\t Descripción: " + dbProducto3.getDescripcion());
        System.out.println("\t Precio: " + dbProducto3.getPrecio());
        
        System.out.println(" Catalogo de Ordenes");
        
        int ordn1Id = orden1.getIdOrden();
        int ordn2Id = orden2.getIdOrden();

        Orden dborden1 = em.find(Orden.class, ordn1Id);
        System.out.println("Datos de Orden 1");
        System.out.println("\t Id: " + dborden1.getIdOrden());
        System.out.println("\t Fecha: " + dborden1.getFechaOrden());
        
        Orden dborden2 = em.find(Orden.class, ordn2Id);
        System.out.println("Datos de  Orden 2");
        System.out.println("\t Id: " + dborden2.getIdOrden());
        System.out.println("\t Fecha: " + dborden2.getFechaOrden());
        
        System.out.println(" Lineas de Orden ");
        LineaordenPK lord1= lineorden1.getLineaordenPK();
        Lineaorden dblineaordn1=em.find(Lineaorden.class, lord1);
        System.out.println("Datos de  Linea de Orden 1");
        System.out.println("\t" + dblineaordn1.getLineaordenPK());        
        System.out.println("\t" + dblineaordn1.getOrden());
        System.out.println("\t" + dblineaordn1.getProducto());
        System.out.println("\t Cantidad: " + dblineaordn1.getCantidad());
        
        LineaordenPK lord2= lineorden2.getLineaordenPK();
        Lineaorden dblineaordn2=em.find(Lineaorden.class, lord2);
        System.out.println("Datos de  Linea de Orden 2");
        System.out.println("\t" + dblineaordn2.getLineaordenPK());        
        System.out.println("\t" + dblineaordn2.getOrden());
        System.out.println("\t" + dblineaordn2.getProducto());
        System.out.println("\t Cantidad: " + dblineaordn2.getCantidad());
        
        LineaordenPK lord3= lineorden3.getLineaordenPK();
        Lineaorden dblineaordn3=em.find(Lineaorden.class, lord3);
        System.out.println("Datos de  Linea de Orden 3");
        System.out.println("\t" + dblineaordn3.getLineaordenPK());        
        System.out.println("\t" + dblineaordn3.getOrden());
        System.out.println("\t" + dblineaordn3.getProducto());
        System.out.println("\t Cantidad: " + dblineaordn3.getCantidad());
        
        LineaordenPK lord4= lineord4.getLineaordenPK();
        Lineaorden dblineaordn4=em.find(Lineaorden.class, lord4);
        System.out.println("Datos de  Linea de Orden 4");
        System.out.println("\t" + dblineaordn4.getLineaordenPK());        
        System.out.println("\t" + dblineaordn4.getOrden());
        System.out.println("\t" + dblineaordn4.getProducto());
        System.out.println("\t Cantidad: " + dblineaordn4.getCantidad());
        
        try {
            em.getTransaction().commit();
        } catch (Exception e) {
            System.out.println("Error "+ e.getMessage());

        }

        em.close();
        Emf.close();
        
    }
    
}
